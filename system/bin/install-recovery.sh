#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery:36700160:5b9eb4c53d3a9da3efa584f1a9d5aabefbe05f82; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/boot:36700160:757a59d0d277f75487c149b212f1b5ddb21420c7 EMMC:/dev/block/platform/soc/soc.ap-ahb/20600000.sdio/by-name/recovery 5b9eb4c53d3a9da3efa584f1a9d5aabefbe05f82 36700160 757a59d0d277f75487c149b212f1b5ddb21420c7:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
